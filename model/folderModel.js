const mongoose = require('mongoose')
const Schema = mongoose.Schema;
// import { Long } from 'mongodb'

var current_millies = new Date().getTime();

var FolderSchema = new mongoose.Schema({
    _id: {
        type: String
    },
    name: {
        type: String
    },
    type: {
        type: String,
        default: `folder`
    },
    timestamps: {
        type: Number
    },
    owner_id: {
        type: Number
    },
    company_id: {
        type: Number
    },
    content: {
        type: Object
    }
},
{ versionKey: false }
)


var Folder = mongoose.model('Folder', FolderSchema)

module.exports = Folder