const mongoose = require('mongoose')
const Schema = mongoose.Schema;
// import { Long } from 'mongodb'

var current_millies = new Date().getTime();

var DocumentSchema = new mongoose.Schema({
    _id: {
        type: String
    },
    name: {
        type: String
    },
    type: {
        type: String
    },
    folder_id: {
        type: String
    },
    content: {
        type: Object,
    },
    timestamp: {
        type: Number
    },
    owner_id: {
        type: Number
    },
    share: {
        type: Array
    },
    company_id: {
        type: Number
    }
},
{ versionKey: false }
)


var Document = mongoose.model('Document', DocumentSchema)

module.exports = Document