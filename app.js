const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const router = require('./router')
const dotenv = require('dotenv')

dotenv.config()

const app = express()
const port = process.env.PORT || 3000
const host = process.env.HOST

const mongoUrl = 'mongodb://localhost:27017/document-service'
const mongoDB = mongoUrl
const options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false ,
  };
mongoose.connect(mongoDB, options) 
mongoose.Promise = global.Promise
const db = mongoose.connection
db.on("error", console.error.bind(console, "Connection Error Coba Cek Dulu"))

app.use(cors())
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(router)

app.use((err, req, res, next) =>{
    const error = app.get('env') === 'development' ? err : {};
    const status = err.status || 500;
    // response to client
    res.status(status).json({
        error:{
            message: error.message
        }
    })
    // response to ourselves
    console.error(err);
})


app.listen(port, host, () => console,console.log('localhost Sukses Running on port '+port))