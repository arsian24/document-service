const Redis = require('ioredis')
const dotenv = require('dotenv')

dotenv.config()

const port = process.env.PORT_REDIS || 6379
const host = process.env.HOST_REDIS

const redis = Redis.createClient({
    host: host,
    port: port,
    password: ''
})

redis.on('error', err => {
    console.log('Error connect redis' + err);
});


module.exports = redis