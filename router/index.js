const express = require('express')
const serviceController = require('../controller/serviceController')
const authorization = require('../middleware/auth')


const route = express()
route.use(authorization)
route.get('/document-service', serviceController.getDocument)
route.get('/document-service/folder/:folder_id', serviceController.getListFile)
route.get('/document-service/document/:document_id', serviceController.getDetailDocument)
route.post('/document-service/folder', serviceController.setFolder)
route.post('/document-service/document', serviceController.setDocument)
route.delete('/document-service/folder', serviceController.deleteFolder)
route.delete('/document-service/document', serviceController.deleteDocument)

module.exports = route;