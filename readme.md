# Introduce For installation
After you clone this project, do the following:

```bash
    #go into the project
    cd document-service

    npm install

    #for running application u can use nodemon or pm2
```

# POST COLLECTION
```bash
    #you can import post collection with this link
    https://www.getpostman.com/collections/59343c28686efebf54a2

```

# Api Spesification

## Authentication

some Api features must use this authentication

```bash
Request :
- Header :
    - token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKb2pvbm9taWMiLCJpYXQiOjE2MDY2OTYyOTYsImV4cCI6MTYzODIzMjI5NiwiYXVkIjoiam9qb25vbWljLmNvbSIsInN1YiI6Impvam9hcmllZiIsImNvbXBhbnlfaWQiOiIxMzAiLCJ1c2VyX2lkIjoiMTIwIn0.jdnqi7hHmeeQlOJ1o-nZBxynTah-QuDj-SjItbol8XE
```
## Root List

```bash
Request :
- Method : GET
- Endpoint : `http://localhost:3000/document-service`
- Header :
    - Content-Type: application/json
    - token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKb2pvbm9taWMiLCJpYXQiOjE2MDY2OTYyOTYsImV4cCI6MTYzODIzMjI5NiwiYXVkIjoiam9qb25vbWljLmNvbSIsInN1YiI6Impvam9hcmllZiIsImNvbXBhbnlfaWQiOiIxMzAiLCJ1c2VyX2lkIjoiMTIwIn0.jdnqi7hHmeeQlOJ1o-nZBxynTah-QuDj-SjItbol8XE
- Body :
```

Response :

```json 
{
    "error": false,
    "data": [
        {
        "share": [
            1,
            23,
            4232,
            121
        ],
        "_id": "82b07a6f-60cc-4403-8fd2-329ef0de045a",
        "name": "Document Job desc Tech",
        "type": "document",
        "content": {
            "blocks": [
            {
                "type": "paragraph",
                "text": "This is paragraph"
            }
            ]
        },
        "timestamp": 1605081795,
        "owner_id": 123,
        "company_id": 23
        }
    ]
}
```

## Set Folder

```bash
Request :
- Method : POST
- Endpoint : `http://localhost:3000/document-service/folder`
- Header :
    - Content-Type: application/json
    - token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKb2pvbm9taWMiLCJpYXQiOjE2MDY2OTYyOTYsImV4cCI6MTYzODIzMjI5NiwiYXVkIjoiam9qb25vbWljLmNvbSIsInN1YiI6Impvam9hcmllZiIsImNvbXBhbnlfaWQiOiIxMzAiLCJ1c2VyX2lkIjoiMTIwIn0.jdnqi7hHmeeQlOJ1o-nZBxynTah-QuDj-SjItbol8XE
- Body :
```

```json 
{
        "_id": "82b07a6f-60cc-4403-8fd2-329ef0de0d3d",
        "name": "Folder Baru", 
		"type": "folder",
		"content": {},
        "timestamp": 16576232323,
		"owner_id": 123,
      	"company_id": 23
}
```

Response :

```json 
{
  "error": false,
  "message": "folder created",
  "data": {
    "type": "folder",
    "_id": "82b07a6f-60cc-4403-8fd2-329ef0de0d3d",
    "name": "Folder Baru",
    "owner_id": 123,
    "company_id": 23
  }
}
```


## Delete Folder

```bash
Request :
- Method : DELETE
- Endpoint : `http://localhost:3000/document-service/folder`
- Header :
    - Content-Type: application/json
    - token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKb2pvbm9taWMiLCJpYXQiOjE2MDY2OTYyOTYsImV4cCI6MTYzODIzMjI5NiwiYXVkIjoiam9qb25vbWljLmNvbSIsInN1YiI6Impvam9hcmllZiIsImNvbXBhbnlfaWQiOiIxMzAiLCJ1c2VyX2lkIjoiMTIwIn0.jdnqi7hHmeeQlOJ1o-nZBxynTah-QuDj-SjItbol8XE
- Body :
```

```json 
{
    "_id": "82b07a6f-60cc-4403-8fd2-329ef0de0d3d" 
}
```

Response :

```json 
{
  "error": false,
  "message": "Success delete folder"
}
```

## List File Per Folder

```bash
Request :
- Method : GET
- Endpoint : `http://localhost:3000/document-service/folder/82b07a6f-60cc-4403-8fd2-329ef0de0d3e`
- Header :
    - Content-Type: application/json
    - token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKb2pvbm9taWMiLCJpYXQiOjE2MDY2OTYyOTYsImV4cCI6MTYzODIzMjI5NiwiYXVkIjoiam9qb25vbWljLmNvbSIsInN1YiI6Impvam9hcmllZiIsImNvbXBhbnlfaWQiOiIxMzAiLCJ1c2VyX2lkIjoiMTIwIn0.jdnqi7hHmeeQlOJ1o-nZBxynTah-QuDj-SjItbol8XE
- Body :
```

Response :

```json 
{
  "error": false,
  "data": [
    {
      "share": [
        1,
        23,
        4232,
        121
      ],
      "_id": "82b07a6f-60cc-4403-8fd2-329ef0de045a",
      "name": "Document Job desc Tech",
      "type": "document",
      "folder_id": "82b07a6f-60cc-4403-8fd2-329ef0de0d3e",
      "content": {
        "blocks": [
          {
            "type": "paragraph",
            "text": "This is paragraph"
          }
        ]
      },
      "timestamp": 1605081795,
      "owner_id": 123
    }
  ]
}
```

## Set (Create/Update) document

```bash
Request :
- Method : POST
- Endpoint : `http://localhost:3000/document-service/document`
- Header :
    - Content-Type: application/json
    - token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKb2pvbm9taWMiLCJpYXQiOjE2MDY2OTYyOTYsImV4cCI6MTYzODIzMjI5NiwiYXVkIjoiam9qb25vbWljLmNvbSIsInN1YiI6Impvam9hcmllZiIsImNvbXBhbnlfaWQiOiIxMzAiLCJ1c2VyX2lkIjoiMTIwIn0.jdnqi7hHmeeQlOJ1o-nZBxynTah-QuDj-SjItbol8XE
- Body :
```

```json 
{
    "_id": "82b07a6f-60cc-4403-8fd2-329ef0de045z",
    "name": "Document Job desc Tech",
    "type": "document",
    "folder_id": "82b07a6f-60cc-4403-8fd2-329ef0de0d3e", 
    "content": {
        "blocks": [
            {
                "type": "paragraphqqs",
                "text": "This is paragraph"
            }
         ]
    }, 
    "timestamp": 1605081795, 
    "owner_id": 123, 
    "share": [1,23,4232,121], 
    "company_id": 23
}
```

Response :

```json 
{
  "error": false,
  "message": "success update document",
  "data": {
    "document": {
      "share": [
        1,
        23,
        4232,
        121
      ],
      "_id": "82b07a6f-60cc-4403-8fd2-329ef0de045z",
      "name": "Document Job desc Tech",
      "type": "document",
      "folder_id": "82b07a6f-60cc-4403-8fd2-329ef0de0d3e",
      "content": {
        "blocks": [
          {
            "type": "paragraphqqs",
            "text": "This is paragraph"
          }
        ]
      },
      "timestamp": 1605081795,
      "owner_id": 123,
      "company_id": 23
    }
  }
}
```

## Get Detail Document

```bash
Request :
- Method : GET
- Endpoint : `http://localhost:3000/document-service/document/82b07a6f-60cc-4403-8fd2-329ef0de045a`
- Header :
    - Content-Type: application/json
    - token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKb2pvbm9taWMiLCJpYXQiOjE2MDY2OTYyOTYsImV4cCI6MTYzODIzMjI5NiwiYXVkIjoiam9qb25vbWljLmNvbSIsInN1YiI6Impvam9hcmllZiIsImNvbXBhbnlfaWQiOiIxMzAiLCJ1c2VyX2lkIjoiMTIwIn0.jdnqi7hHmeeQlOJ1o-nZBxynTah-QuDj-SjItbol8XE
- Body :
```

Response :

```json 
{
  "error": false,
  "data": {
    "share": [
      1,
      23,
      4232,
      121
    ],
    "_id": "82b07a6f-60cc-4403-8fd2-329ef0de045a",
    "name": "Document Job desc Tech",
    "type": "document",
    "folder_id": "82b07a6f-60cc-4403-8fd2-329ef0de0d3e",
    "content": {
      "blocks": [
        {
          "type": "paragraph",
          "text": "This is paragraph"
        }
      ]
    },
    "timestamp": 1605081795,
    "owner_id": 123,
    "company_id": 23
  }
}
```

## Delete Document

```bash
Request :
- Method : DELETE
- Endpoint : `http://localhost:3000/document-service/document`
- Header :
    - Content-Type: application/json
    - token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKb2pvbm9taWMiLCJpYXQiOjE2MDY2OTYyOTYsImV4cCI6MTYzODIzMjI5NiwiYXVkIjoiam9qb25vbWljLmNvbSIsInN1YiI6Impvam9hcmllZiIsImNvbXBhbnlfaWQiOiIxMzAiLCJ1c2VyX2lkIjoiMTIwIn0.jdnqi7hHmeeQlOJ1o-nZBxynTah-QuDj-SjItbol8XE
- Body :
```

```json 
{
        "_id": "82b07a6f-60cc-4403-8fd2-329ef0de045a" 
}
```

Response :

```json 
{
  "error": false,
  "message": "Success delete document"
}
```


