const Document = require('../model/documentModel')
const Folder = require('../model/folderModel')
const redis = require('../redis')


class DocumentController{
    static async getDocument(req, res){ 
        try {
            const cache = await redis.get('services')
            if(cache){
                res.status(200).json({
                    error: false,
                    data: JSON.parse(cache)}) 
            } else {
                let data = []
                let fieldDocument = { '_id': 1, 'name': 1 , 'type': 1,'content': 1,
                'timestamp': 1,'owner_id': 1,'share': 1,'company_id': 1, };
                let documents = await Document.find({}).select(fieldDocument)
                await redis.set('services', JSON.stringify(documents))
                res.status(200).json({
                    error: false,
                    data:documents
                })
            }
        } catch (error) {
            console.log(error)
            res.status(500).json({error})
        }
    }

    static async getListFile(req, res){
        let { folder_id } = req.params
        try {
            const cache = await redis.get('folder')
            let fieldDocument = { '_id': 1, 'name': 1 , 'type': 1,'content': 1,
                'timestamp': 1,'owner_id': 1,'share': 1,'folder_id': 1, };
            if(cache){
                let data = JSON.parse(cache)
                let isSame = (value) => value == folder_id
                let datas = data.map(elm => {
                               return elm.folder_id
                }) 
                if(datas.every(isSame)){
                    res.status(200).json({
                        error: false,
                        data:data
                    })
                } else {
                    data = await Document.find({folder_id}).select(fieldDocument)
                    await redis.set('folder', JSON.stringify(data))
                    res.status(200).json({
                    error: false,
                    data:data
                })    
                }
            } else {
                
                const data = await Document.find({folder_id:folder_id}).select(fieldDocument)
                await redis.set('folder', JSON.stringify(data))
                res.status(200).json({
                    error: false,
                    data:data
                })
            }
        } catch (error) {
            res.status(500).json({
                error
            })
        }
    }

    static async getDetailDocument(req, res){
        let { document_id } = req.params
        try {
            const cache = await redis.get('document')
            if(cache){
                console.log(cache)
                let data = JSON.parse(cache)
                if(data._id == document_id){
                    res.status(200).json({
                        error: false,
                        message: "Success get document",
                        data:data
                    })    
                } else {
                    data = await Document.findOne({_id: document_id})
                    await redis.set('document', JSON.stringify(data))
                    res.status(200).json({
                        error: false,
                        message: "Success get document",
                        data:data
                    })        
                }
            } else {
                console.log(document_id)
                const data = await Document.findOne({_id: document_id})
                await redis.set('document', JSON.stringify(data))
                res.status(200).json({
                    error: false,
                    message: "Success get document",
                    data:data
                })
            }
        } catch (error) {
            console.log(error)
            res.status(500).json({
                error
            })
        }
    }

    static async setFolder(req, res){
        let {_id, name, type,content,timestamp,owner_id,company_id} = req.body
        
        try {
            const checker = await Folder.findOne({_id})
            if(checker){
                let folder = ({
                    _id,
                    name,
                    type,
                    timestamp,
                    owner_id,
                    company_id,
                    content,
                })
                // let data = await Folder.replaceOne(folder)
                await Folder.findByIdAndUpdate(_id, folder)
                res.status(201).json({
                    error: false,
                    message: 'folder updated',
                    data:folder
                })
            } else {
                let folder = new Folder({
                    _id,
                    name,
                    type,
                    timestamp,
                    owner_id,
                    company_id,
                    content,
                })
                let data = await folder.save()
                res.status(201).json({
                    error: false,
                    message: 'folder created',
                    data:data
                })
            }
        } catch (error) {
            console.log(error)
            res.status(500).json({
                error
            })
        }
    }

    static async setDocument(req, res){
        let { _id, name, type, folder_id, content, timestamp, owner_id, share, company_id } = req.body
        try {
            const newDocument = new Document({
                _id,
                name,
                type,
                folder_id,
                content,
                timestamp,
                owner_id,
                share,
                company_id
            })
            
            
            let document = await Document.findById(_id)
            if (document == null){
                document = await newDocument.save()   
                res.status(201).json({
                    error: false,
                    message: 'success set document',
                    data: {
                        document:document
                    }
                })
            }else{
                await Document.findByIdAndUpdate(_id, newDocument)
                res.status(201).json({
                    error: false,
                    message: 'success update document',
                    data: {
                        document:newDocument
                    }
                })
            }
            
        } catch (error) {
            console.log(error);
            res.status(500).json({
                error
            })
        }
    }

    static async deleteFolder(req, res){
        let { _id } = req.body
        try {
            let cache = await redis.get('services')
            let cacheData = JSON.parse(cache)
            for (let i = 0; i < cacheData.length; i++) {
                if(cacheData[i]._id == _id){
                    cacheData.splice(i, 1)
                }
            }
            await redis.set('services', JSON.stringify(cacheData))
            let data = await Folder.findOneAndDelete({_id})
            res.status(200).json({
                error: false,
                message: 'Success delete folder'
            })
        } catch (error) {
            res.status(500).json({
                error: 'internal server error'
            })
        }
    }

    static async deleteDocument(req, res){
        let { _id } = req.body
        try {
            let cache = await redis.get('services')
            let cacheData = JSON.parse(cache)
            for (let i = 0; i < cacheData.length; i++) {
                if(cacheData[i]._id == _id){
                    cacheData.splice(i, 1)
                }
            }
            await redis.set('services', JSON.stringify(cacheData))
            let data = await Document.findOneAndDelete({_id})
            res.status(200).json({
                error: false,
                message: 'Success delete document'
            })
        } catch (error) {
            res.status(500).json({
                error: 'internal server error'
            })
        }
    }
}

module.exports = DocumentController